## get start
```
node.js v9.11.1

npm i -g ganache-cli@6.1.8 truffle@4.1.14
```

another pane
```
// open ganache-cli
ganache-cli

```

comamnd
```
// install library
$ npm i

// check compile and build json file
$ truffle compile

// deploy
$ truffle migrate

```


## deploy
```
$ touch .env
$ echo MNEMONIC=hoge hoge hoge hoge hoge hoge hoge hoge hoge hoge >> .env
$ echo INFURA_ACCESS_TOKEN=YOUR_INFURA_ACCCESS_TOKEN >> .env
```

build
```
$ truffel compile
```

rinkeby testnet
``` 
$ truffle migrate network --rinkeby
```

ropsten testnet // not yet
```
$ truffle migrate network --ropsten
```

main testnet // not yet 
```
$ truffle migrate network --main
```

## test

``` 
$ truffle test network --test
```
or

```
$ truffle console network --test
$ truffle(test)> test
```

