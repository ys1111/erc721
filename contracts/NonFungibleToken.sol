pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/token/ERC721/ERC721Token.sol";

contract NonFungibleToken is ERC721Token, Ownable {
  uint256 internal nextTokenId = 0;

  constructor() public ERC721Token("Cryptoeconomics Labs", "CEL") {}

  function mint(address _to, uint256 _tokenId) external onlyOwner {    
    super._mint(_to, _tokenId);
  }

  function burn(uint256 _tokenId) external onlyOwner {
    address owner = tokenOwner[_tokenId];
    require(owner != address(0));
    super._burn(owner, _tokenId);
  }

  function executeTransfer(address _from, address _to, uint256 _tokenId) external onlyOwner {
    super.removeTokenFrom(_from, _tokenId);
    super.addTokenTo(_to, _tokenId);
  }
}
