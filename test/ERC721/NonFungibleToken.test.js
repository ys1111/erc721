const BigNumber = web3.BigNumber;
const _ = require('lodash');
const NonFungibleToken = artifacts.require('NonFungibleToken.sol');
const { assertRevert } = require('../helpers/assertRevert.js');
const { injectInTruffle } = require('sol-trace');

injectInTruffle(web3, artifacts);

require('chai')
  .use(require('chai-bignumber')(BigNumber))
  .should();

contract('NonFungibleToken as MintBurnableERC721', async function (accounts) {
  const name = 'Cryptoeconomics Labs';
  const symbol = 'CEL';
  const creator = accounts[0];
  // const ZERO_ADDRESS = '0x000000000000000000000';
  const firstTokenId = 100;
  const secondTokenId = 200;
  const thirdTokenId = 300;

  beforeEach(async function () {
    this.CELtoken = await NonFungibleToken.new({from: creator})
  });

  describe('like a full ERC721', function () {
    beforeEach(async function () {
        await this.CELtoken.mint(creator, firstTokenId, { from: creator });
        await this.CELtoken.mint(creator, secondTokenId, { from: creator });
        await this.CELtoken.mint(accounts[1], thirdTokenId, { from: creator })
    });

    describe('mint', function () {
      const to = accounts[1];

      it('adjusts owner tokens by index', async function () {
        const tokenId1 = await this.CELtoken.tokenOfOwnerByIndex(creator, 0);
        const tokenId2 = await this.CELtoken.tokenOfOwnerByIndex(to, 0);

        tokenId1.toNumber().should.be.equal(firstTokenId);
        tokenId2.toNumber().should.be.equal(thirdTokenId);
      });

      it('adjusts all tokens by index', async function () {
        const newToken = await this.CELtoken.tokenByIndex(1);
        newToken.toNumber().should.be.equal(secondTokenId);
      });
    });
  
    describe('burn', function () {
      beforeEach(async function() {
        await this.CELtoken.burn(firstTokenId, { from: creator });
      });

      it('removes that token from the token list of the owner', async function () {
        const token = await this.CELtoken.tokenOfOwnerByIndex(creator, 0);
        token.toNumber().should.be.equal(secondTokenId);
      });

      it('adjusts all tokens list', async function () {
        const token = await this.CELtoken.tokenByIndex(0);
        token.toNumber().should.be.equal(thirdTokenId);
        });

      it('burns all tokens', async function () {
        await this.CELtoken.burn(secondTokenId, { from: creator });
        await this.CELtoken.burn(thirdTokenId, { from: creator });
        const total = await this.CELtoken.totalSupply();
        total.toNumber().should.be.equal(0);
        await assertRevert(this.CELtoken.tokenByIndex(0));
      });
    });  
   
    // describe('removeTokenFrom', function () {
    //   beforeEach(async function () {
    //     await this.CStoken._removeTokenFrom(creator, firstTokenId, { from: creator });
    //   });

    //   it('has been removed', async function () {
    //     await assertRevert(this.CStoken.tokenOfOwnerByIndex(creator, 1));
    //   });

    //   it('adjusts token list', async function () {
    //     const token = await this.CStoken.tokenOfOwnerByIndex(creator, 0);
    //     token.toNumber().should.be.equal(thirdTokenId);
    //   });

    //   it('adjusts owner count', async function () {
    //     const count = await this.CStoken.balanceOf(creator);
    //     count.toNumber().should.be.equal(1);
    //   });

    //   it('does not adjust supply', async function () {
    //     const total = await this.CStoken.totalSupply();
    //     total.toNumber().should.be.equal(2);
    //   });
    // });

    describe('metadata', function () {
      it('has a name', async function () {
        const tokenName = await this.CELtoken.name();
        tokenName.should.be.equal(name);
      });

      it('has a symbol', async function () {
        const tokenSymbol = await this.CELtoken.symbol();
        tokenSymbol.should.be.equal(symbol);
      });
    });

    describe('totalSupply', function () {
      it('returns total token supply', async function () {
        const totalSupply = await this.CELtoken.totalSupply();
        totalSupply.should.be.bignumber.equal(3);
      });
    });

    describe('tokenOfOwnerByIndex', function () {
      const owner = creator;
      const another = accounts[2];

      describe('when the given index is lower than the amount of tokens owned by the given address', function () {
        it('returns the token ID placed at the given index', async function () {
          const tokenId = await this.CELtoken.tokenOfOwnerByIndex(owner, 0);
          tokenId.should.be.bignumber.equal(firstTokenId);
        });
      });

      describe('when the index is greater than or equal to the total tokens owned by the given address', function () {
        it('reverts', async function () {
          await assertRevert(this.CELtoken.tokenOfOwnerByIndex(owner, 2));
        });
      });

      describe('when the given address does not own any token', function () {
        it('reverts', async function () {
          await assertRevert(this.CELtoken.tokenOfOwnerByIndex(another, 0));
        }); 
      });

      describe('after transferring all tokens to another user', function () {
        beforeEach(async function () {
          await this.CELtoken.transferFrom(owner, another, firstTokenId, { from: owner });
          await this.CELtoken.transferFrom(owner, another, secondTokenId, { from: owner });
        });

        it('returns correct token IDs for target', async function () {
          const count = await this.CELtoken.balanceOf(another);
          count.toNumber().should.be.equal(2);
          const tokensListed = await Promise.all(_.range(2).map(i => this.CELtoken.tokenOfOwnerByIndex(another, i)));
          tokensListed.map(t => t.toNumber()).should.have.members([firstTokenId, secondTokenId]);
        });

        it('returns empty collection for original owner', async function () {
          const count = await this.CELtoken.balanceOf(owner);
          count.toNumber().should.be.equal(0);
          await assertRevert(this.CELtoken.tokenOfOwnerByIndex(owner, 0));
        });
      }); 
    });

    describe('tokenByIndex', function () {
      it('should return all tokens', async function () {
        const tokensListed = await Promise.all(_.range(2).map(i => this.CELtoken.tokenByIndex(i)));
        tokensListed.map(t => t.toNumber()).should.have.members([firstTokenId, secondTokenId]);
      });

      it('should revert if index is greater than supply', async function () {
        await assertRevert(this.CELtoken.tokenByIndex(3));
      });

      [firstTokenId, secondTokenId, thirdTokenId].forEach(function (tokenId) {
        it(`should return all tokens after burning token ${tokenId} and minting new tokens`, async function () {
          const owner = creator;
          const newTokenId = 400;
          const anotherNewTokenId = 500;

          await this.CELtoken.burn(tokenId, { from: owner });
          await this.CELtoken.mint(owner, newTokenId, { from: owner });
          await this.CELtoken.mint(owner, anotherNewTokenId, { from: owner });

          const count = await this.CELtoken.totalSupply();
          count.toNumber().should.be.equal(4);

          const tokensListed = await Promise.all(_.range(4).map(i => this.CELtoken.tokenByIndex(i)));
          const expectedTokens = _.filter(
            [firstTokenId, secondTokenId, thirdTokenId, newTokenId, anotherNewTokenId],
            x => (x !== tokenId)
          );
          tokensListed.map(t => t.toNumber()).should.have.members(expectedTokens);
        });
      });
    });
    
  });

  describe('mint and excuteTransfer', function () {
    const owner = accounts[0]
    const alice = accounts[7]
    const bob = accounts[8]
    const customer = accounts[9]

    beforeEach(async function () {
      await this.CELtoken.mint(alice, firstTokenId, { from: owner });
      await this.CELtoken.mint(bob, secondTokenId, { from: owner });
    });

    it('execute transfer alice to customer', async function () {
      await assertRevert(this.CELtoken.tokenOfOwnerByIndex(customer, 0));
      await this.CELtoken.executeTransfer(alice, customer, firstTokenId, { from: owner })
      const transferedTokenId = await this.CELtoken.tokenOfOwnerByIndex(customer, 0);

      await assertRevert(this.CELtoken.tokenOfOwnerByIndex(alice, 0));
      transferedTokenId.toNumber().should.be.equal(firstTokenId);
    });
  });
});